var express = require('express');
var router = express.Router();
var Categoria = require('../../model/Categoria');

/**
 * @api {post} / Busca de Categoria
 * @apiName POST-CATEGORIA
 * @apiGroup Categoria
 * @apiParam {Object} query Objeto com campos da busca
 * @apiParam {Object} skip Pulo de resultados
 * @apiParam {Object} limit Limite de resultados
 * @apiParam {Object} sort Campo a ser ordenado
 * @apiSuccess {Object} categorias Array de categorias.
 * @apiError 400 Bad Request.
 * @apiError 500 {Object} error Erro interno.
 * @apiError 404 Not Found.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "categorias": [ ... ]
 *     }
 */
router.post('/', function(req,res,next){

  //inicializando parâmetros skip (pulo), limit (limite) e sort (ordenação)
  var skip = parseInt(req.body.skip || 0);
  var limit = parseInt(req.body.limit || 0);
  var sort = req.body.sort || {nome:-1};

  //buscando categoria a partir da query
  Categoria.find(req.body.query).sort(sort).skip(skip).limit(limit).exec(function(err, categorias){

    if(err){
      return next(err);
    }

    return res.json({categorias:categorias});
  });

});



module.exports = router;
