var express = require('express');
var router = express.Router();
var Aluno = require('../../model/Aluno');
var bcrypt = require('bcrypt');
var config = require('../../config');
var _ = require('lodash');

/**
 * @api {post} / Autenticação de aluno
 * @apiName authenticateUser-ALUNO
 * @apiGroup Aluno
 * @apiParam {Number} matricula
 * @apiParam {String} senha
 * @apiSuccess {String} token Token do aluno.
 * @apiError 400 Bad Request.
 * @apiError 500 {Object} error Erro interno.
 * @apiError 401 Erro de autenticação.
 * @apiError 404 Not Found.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "token": "Token JWT"
 *     }
 */
router.post('/auth', function(req,res,next){

  //verifica parâmetros
  if(!req.body.matricula || !req.body.senha){
    return res.sendStatus(400);
  }

  //console.log("matricula", req.body.matricula);
  //console.log("senha", req.body.senha);

  //autentica usuário e retorna token
  Aluno.authenticateUser(req.body.matricula, req.body.senha, function(err, status, result){

      if(err){
        return res.sendStatus(status);
      }

      if(status === 401){
        return res.sendStatus(401);
      }

      return res.json({token:result});
  });

});

/**
 * @api {post} / Criação de aluno
 * @apiName POST-ALUNO
 * @apiGroup Aluno
 * @apiParam {Object} aluno
 * @apiSuccess {Object} aluno salvo no banco com ObjectID.
 * @apiError 400 Bad Request.
 * @apiError 500 {Object} error Erro interno.
 * @apiError 404 Not Found.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "aluno": {
                  ...
                }
 *     }
 */
router.post('/new', function(req, res, next){
  //verifica parâmetros
  if(!req.body.aluno){
    return res.sendStatus(400);
  }

  //cria hash com senha do aluno
  bcrypt.hash(req.body.aluno.senha, config.saltRoundsBCrypt, function(err, hash) {

    //verifica erros
    if(err){
      next(err);
    }

    //cria aluno com nova senha e salva no banco
    var alunoModel = new Aluno(req.body.aluno);
    alunoModel.senha = hash;
    alunoModel.save(function(err, alunoSaved){

      if(err){
        return next(err);
      }

      return res.json(alunoSaved);
    });

  });

});

/**
 * @api {post} / Alteração de dados de aluno
 * @apiName PUT-ALUNO
 * @apiGroup Aluno
 * @apiHeader (X-Auth) {String} token Token de acesso.
 * @apiParam {Object} aluno Aluno com ID e campos a serem atualizados
 * @apiSuccess {Object} aluno Aluno atualizado no banco com ObjectID.
 * @apiError 400 Bad Request.
 * @apiError 500 {Object} error Erro interno.
 * @apiError 404 Not Found.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "aluno": {
                  ...
                }
 *     }
 */
router.post('/update', require('../../util/filter-auth-aluno') ,function(req, res, next){

  //Verifica parâmetros
  if(!req.body.aluno || !req.body.aluno._id){
    return res.sendStatus(400);
  }

  //Procura aluno no banco pelo _id
  Aluno.findOne({_id:req.body.aluno._id}, function(err, aluno){

    //verifica se aluno existe
    if(!aluno || err){
      return res.sendStatus(204);
    }

    delete req.body.aluno.senha;

    //atualiza aluno
    aluno = _.extend(aluno, req.body.aluno);

    //salva aluno no banco
    aluno.save(function(err, alunoUpdated){
      if(err){
        console.log("matricula existente", err);
        return next(err);
      }

      return res.json(alunoUpdated);
    });

  });

});

router.post('/password', require('../../util/filter-auth-aluno') ,function(req, res, next){

  //Verifica parâmetros
  if(!req.body.aluno || !req.body.aluno._id || !req.body.aluno.senha || !req.body.aluno.novaSenha){
    return res.sendStatus(400);
  }

  //Altera senha do aluno
  Aluno.alterarSenha(req.body.aluno, function(err, aluno){

    //verifica se aluno existe
    if(!aluno || err){
      return res.sendStatus(204);
    }

    return res.json(aluno);

  });

});

/**
 * @api {get} / Busca por id
 * @apiName GET-ALUNO
 * @apiGroup Aluno
 * @apiHeader (X-Auth) {String} token Token de acesso.
 * @apiParam {Object} idAluno ID do aluno
 * @apiSuccess {Object} aluno Aluno do banco com todos seus campos.
 * @apiError 400 Bad Request.
 * @apiError 500 {Object} error Erro interno.
 * @apiError 404 Not Found.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "aluno": {
                  ...
                }
 *     }
 */
router.get('/', require('../../util/filter-auth-aluno') ,function(req, res, next){

  var query = {_id:req.auth._id};

  if(req.query.matricula){
    query = {matricula:req.query.matricula};
  }

  //Procura aluno no banco pelo _id ou matricula
  Aluno.findOne(query, function(err, aluno){

    //verifica se aluno existe
    if(!aluno || err){
      return res.sendStatus(204);
    }

    return res.json({aluno:aluno});

  });

});

module.exports = router;
