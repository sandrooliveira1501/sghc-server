var express = require('express');
var router = express.Router();
var Curso = require('../../model/Curso');

/**
 * @api {post} / Busca de Curso
 * @apiName POST-CURSO
 * @apiGroup Curso
 * @apiParam {Object} query Objeto com campos da busca
 * @apiParam {Object} skip Pulo de resultados
 * @apiParam {Object} limit Limite de resultados
 * @apiParam {Object} sort Campo a ser ordenado
 * @apiSuccess {Object} curso Array de curso.
 * @apiError 400 Bad Request.
 * @apiError 500 {Object} error Erro interno.
 * @apiError 404 Not Found.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "curso": [ ... ]
 *     }
 */
router.post('/', function(req,res,next){

  //inicializando parâmetros skip (pulo), limit (limite) e sort (ordenação)
  var skip = parseInt(req.body.skip || 0);
  var limit = parseInt(req.body.limit || 0);
  var sort = req.body.sort || {nome:-1};

  //buscando categoria a partir da query
  Curso.find(req.body.query).sort(sort).skip(skip).limit(limit).exec(function(err, cursos){

    if(err){
      return next(err);
    }

    return res.json({cursos:cursos});
  });

});



module.exports = router;
