var express = require('express');
var router = express.Router();
var Atividade = require('../../model/Atividade');
var _ = require('lodash');
var fs = require('fs');
var multiparty = require('connect-multiparty');
var path = require('path');
var util = require('../../util');

router.route('/certificado')
    .post(multiparty(), function(req,res,next){

        //verifica parâmetros
        if(!req.body.idAtividade){
            return res.sendStatus(400);
        }

        //busca atividade
        Atividade.findById(req.body.idAtividade, function(err, atividade){

            if(!atividade){
                return res.sendStatus(204);
            }

            //Verificar Permissão
            if(req.auth._id.toString() !== atividade.aluno.toString()){
              return res.sendStatus(401);
            }

            res.setHeader("Access-Control-Allow-Origin", "*");

            if(!req.files || !req.files.file) return res.sendStatus(400);

            var arquivo = req.files.file;
            var temporario = req.files.file.path;

            //var novo = __dirname + "/uploads/" + util.generateUUID() + ".png"
            console.log(temporario.indexOf(".pdf") > -1);
            var ext = ".pdf";

            var filePathName = util.generateFileName(ext);

            var novo = util.generateFullFilePath(filePathName);

            fs.rename(temporario, novo, function(errWriteFile){
                if(errWriteFile || req.body.simulateErrorFile){
                    console.log(errWriteFile);
                    return res.sendStatus(500);
                }

                atividade.arquivo = filePathName;
                atividade.save(function(errDatabase, atividadeUpdated){

                  if(errDatabase ||  req.body.simulateErrorDatabase){
                    return res.sendStatus(500);
                  }

                  return res.json(atividadeUpdated);
                });

            });

        })

    });

module.exports = router;
