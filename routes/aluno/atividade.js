var express = require('express');
var router = express.Router();
var Atividade = require('../../model/Atividade');
var _ = require('lodash');

/**
 * @api {post} / Busca de Atividade
 * @apiName SEARCH-ATIVIDADE
 * @apiGroup Atividade
 * @apiParam {Object} query Objeto com campos da busca
 * @apiParam {Object} skip Pulo de resultados
 * @apiParam {Object} limit Limite de resultados
 * @apiParam {Object} sort Campo a ser ordenado
 * @apiSuccess {Object} atividades Array de atividades.
 * @apiError 400 Bad Request.
 * @apiError 500 {Object} error Erro interno.
 * @apiError 404 Not Found.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "atividades": [ ... ]
 *     }
 */
router.post('/search', function(req,res,next){

  //inicializando parâmetros skip (pulo), limit (limite) e sort (ordenação)
  var skip = parseInt(req.body.skip || 0);
  var limit = parseInt(req.body.limit || 0);
  var sort = req.body.sort || {data:-1};

  var query = {};
  if(req.body.query){
    query = req.body.query;
  }
  query.aluno = req.auth._id;

  //buscando atividades a partir da query
  Atividade.find(query).sort(sort).skip(skip).limit(limit).exec(function(err, atividades){

    if(err){
      return next(err);
    }

    return res.json({atividades:atividades});
  });

});


/**
 * @api {post} / Criação de Atividade
 * @apiName POST-ATIVIDADE
 * @apiGroup Atividade
 * @apiParam {Object} atividade
 * @apiSuccess {Object} atividade Atividade com _id do banco.
 * @apiError 400 Bad Request.
 * @apiError 500 {Object} error Erro interno.
 * @apiError 401 Erro de autenticação.
 * @apiError 404 Not Found.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "atividade": { ... }
 *     }
 */
router.post('/', function(req,res,next){

  //verificação de parâmetros
  if(!req.body.atividade){
    return res.sendStatus(400);
  }

  //criação de atividade
  var atividade = new Atividade(req.body.atividade);
  atividade.aluno = req.auth._id;
  //salvando atividade no banco
  atividade.save(function(err, atividade){
    if(err){
      return next(err);
    }

    return res.json(atividade);
  });

});

/**
 * @api {put} / Edição de Atividade
 * @apiName PUT-ATIVIDADE
 * @apiGroup Atividade
 * @apiParam {Object} atividade Atividade com _id.
 * @apiSuccess {Object} atividade Atividade com _id do banco.
 * @apiError 400 Bad Request.
 * @apiError 500 {Object} error Erro interno.
 * @apiError 401 Erro de autenticação.
 * @apiError 404 Not Found.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "atividade": { ... }
 *     }
 */
router.put('/', function(req,res,next){

  //Verifica parâmetros
  if(!req.body.atividade || !req.body.atividade._id){
    return res.sendStatus(400);
  }

  //procura atividade no banco pelo _id
  Atividade.findOne({_id:req.body.atividade._id}, function(err, atividade){

    //verifica se atividade existe e tem id do aluno
    if(!atividade || err || atividade.aluno.toString() !== req.auth._id.toString()){
      return res.sendStatus(204);
    }

    //atualiza atividade
    atividade = _.extend(atividade, req.body.atividade);

    //salva atividade no banco
    atividade.save(function(err, atividadeUpdated){
      if(err){
        return next(err);
      }

      return res.json(atividadeUpdated);
    });

  });

});


/**
 * @api {delete} / Remoção de Atividade
 * @apiName DELETE-ATIVIDADE
 * @apiGroup Atividade
 * @apiParam {String} idAtividade _id da atividade.
 * @apiError 400 Bad Request.
 * @apiError 500 {Object} error Erro interno.
 * @apiError 401 Erro de autenticação.
 * @apiError 404 Not Found.
 */
router.delete('/', function(req,res,next){

  //Verifica parâmetros
  console.log(req.body);
  if(!req.body.idAtividade){
    return res.sendStatus(400);
  }

  //remove atividade no banco pelo _id
  Atividade.remove({_id:req.body.idAtividade, aluno:req.auth._id}, function(err, result){

    if(err){
      return next(err);
    }

    return res.json(result);
  });

});


module.exports = router;
