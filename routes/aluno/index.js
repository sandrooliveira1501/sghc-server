var express = require('express');
var router = express.Router();

router.use('/', require('./aluno'));
router.use('/categoria', require('./categoria'));
router.use('/curso', require('./curso'));
router.use('/atividade', require('../../util/filter-auth-aluno') ,require('./atividade'));
router.use('/atividade', require('../../util/filter-auth-aluno') ,require('./atividade-certificado'));

//router.use('/public', require('./public'))

module.exports = router;
