var express = require('express');
var router = express.Router();
var Administrador = require('../../model/Administrador');
/**
 * @api {post} / Autenticação de Administrador
 * @apiName AUTH-ADMIN
 * @apiGroup Administrador
 * @apiParam {Number} login
 * @apiParam {String} senha
 * @apiSuccess {String} token Token do Administrador.
 * @apiError 400 Bad Request.
 * @apiError 500 {Object} error Erro interno.
 * @apiError 401 Erro de autenticação.
 * @apiError 404 Not Found.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "token": "Token JWT"
 *     }
 */
router.post('/auth', function(req,res,next){

  if(!req.body.login || !req.body.senha){
    return res.sendStatus(400);
  }

  //console.log("login", req.body.login);
  //console.log("senha", req.body.senha);

  Administrador.authenticateUser(req.body.login, req.body.senha, function(err, status, result){

      if(err){
        return res.sendStatus(status);
      }
      if(status === 401){
        return res.sendStatus(401);
      }

      return res.json({token:result});
  });

});


module.exports = router;
