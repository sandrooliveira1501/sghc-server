var express = require('express');
var router = express.Router();
var Categoria = require('../../model/Categoria');
var _ = require('lodash');

/**
 * @api {post} / Criação de Categoria
 * @apiName POST-CATEGORIA
 * @apiGroup Categoria
 * @apiParam {Object} categoria
 * @apiSuccess {Object} categoria Categoria com _id do banco.
 * @apiError 400 Bad Request.
 * @apiError 500 {Object} error Erro interno.
 * @apiError 401 Erro de autenticação.
 * @apiError 404 Not Found.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "categoria": { ... }
 *     }
 */
router.post('/', function(req,res,next){

  //verificação de parâmetros
  if(!req.body.categoria){
    return res.sendStatus(400);
  }

  //criação de categoria
  var categoria = new Categoria(req.body.categoria);

  //salvando categoria no banco
  categoria.save(function(err, categoria){
    if(err){
      return next(err);
    }

    return res.json(categoria);
  });

});

/**
 * @api {put} / Edição de Categoria
 * @apiName PUT-CATEGORIA
 * @apiGroup Categoria
 * @apiParam {Object} categoria Categoria com _id.
 * @apiSuccess {Object} categoria Categoria com _id do banco.
 * @apiError 400 Bad Request.
 * @apiError 500 {Object} error Erro interno.
 * @apiError 401 Erro de autenticação.
 * @apiError 404 Not Found.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "categoria": { ... }
 *     }
 */
router.put('/', function(req,res,next){

  //Verifica parâmetros
  if(!req.body.categoria || !req.body.categoria._id){
    return res.sendStatus(400);
  }

  //procura categoria no banco pelo _id
  Categoria.findOne({_id:req.body.categoria._id}, function(err, categoria){

    //verifica se curso existe
    if(!categoria || err){
      return res.sendStatus(204);
    }

    //atualiza categoria
    categoria = _.extend(categoria, req.body.categoria);

    //salva categoria no banco
    categoria.save(function(err, categoriaUpdated){
      if(err){
        return next(err);
      }

      return res.json(categoriaUpdated);
    });

  });

});


module.exports = router;
