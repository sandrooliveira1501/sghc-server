var express = require('express');
var router = express.Router();
var Curso = require('../../model/Curso');
var _ = require('lodash');

/**
 * @api {post} / Criação de Curso
 * @apiName POST-CURSO
 * @apiGroup Curso
 * @apiParam {Object} curso
 * @apiSuccess {Object} curso Curso com _id do banco.
 * @apiError 400 Bad Request.
 * @apiError 500 {Object} error Erro interno.
 * @apiError 401 Erro de autenticação.
 * @apiError 404 Not Found.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "curso": { ... }
 *     }
 */
router.post('/', function(req,res,next){

  //verificação de parâmetros
  if(!req.body.curso){
    return res.sendStatus(400);
  }

  //criação de curso
  var curso = new Curso(req.body.curso);

  //salvando curso no banco
  curso.save(function(err, curso){
    if(err){
      return next(err);
    }

    return res.json(curso);
  });

});

/**
 * @api {put} / Edição de Curso
 * @apiName PUT-CURSO
 * @apiGroup Curso
 * @apiParam {Object} curso Curso com _id.
 * @apiSuccess {Object} curso Curso com _id do banco.
 * @apiError 400 Bad Request.
 * @apiError 500 {Object} error Erro interno.
 * @apiError 401 Erro de autenticação.
 * @apiError 404 Not Found.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "curso": { ... }
 *     }
 */
router.put('/', function(req,res,next){

  //Verifica parâmetros
  if(!req.body.curso || !req.body.curso._id){
    return res.sendStatus(400);
  }

  //procura curso no banco pelo _id
  Curso.findOne({_id:req.body.curso._id}, function(err, curso){

    //verifica se curso existe
    if(!curso || err){
      return res.sendStatus(204);
    }

    //atualiza curso
    curso = _.extend(curso, req.body.curso);

    //salva curso no banco
    curso.save(function(err, cursoUpdated){
      if(err){
        return next(err);
      }

      return res.json(cursoUpdated);
    });

  });

});


module.exports = router;
