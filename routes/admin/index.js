var express = require('express');
var router = express.Router();

router.use('/', require('./admin'));
router.use('/categoria', require('../../util/filter-auth-admin'), require('./categoria'));
router.use('/curso', require('../../util/filter-auth-admin'), require('./curso'));

//router.use('/public', require('./public'))

module.exports = router;
