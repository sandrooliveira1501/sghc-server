var express = require('express');
var router = express.Router();
var aluno = require('./aluno');

router.use('/aluno', require('./aluno'));
router.use('/admin', require('./admin'));

//router.use('/public', require('./public'))

/**
 * @api {get} / Requisição de Teste
 * @apiName SGHC
 * @apiGroup Teste
 *
 *
 * @apiSuccess {String} Ok.
 * @apiError 404 Not Found.
 */
router.get('/', function(req,res,next){
  return res.json({message:"Ok"});
});

module.exports = router;
