//using  Immediately Invoked Function Expression (IIFE)

(function(){
    var mongoose = require('mongoose')
    mongoose.set('debug', true);
    console.log("NODE_ENV", process.env.NODE_ENV);
    if(process.env.NODE_ENV && 'production' ===  process.env.NODE_ENV.toString()){
      mongoose.connect('mongodb://mongodb:mongodb@ds061246.mlab.com:61246/sghc', function () {
          console.log('mongodb connected')
      });
    }else{
      mongoose.connect('mongodb://localhost/sghc', function () {
          console.log('mongodb connected')
      });
    }

    module.exports = mongoose
})()
