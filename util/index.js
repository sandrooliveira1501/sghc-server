/**
 * Created by alexsandro.
 */
(function(){

    var _ = require('lodash');
    var uuid = require('node-uuid');
    var path = require('path');

    //TODO conseguir pegar o root do projeto
    const FILE_DIRECTORY = path.resolve(__dirname + "/../uploads/");

    function generateUUID(){
        return uuid.v4();
    }

    function generateFileName(ext){
        if(!ext) ext = ".pdf";

        return uuid.v4() + ext;
    }

    function generateFullFilePath(filePathName){
        return FILE_DIRECTORY + "/" + filePathName;
    }

    module.exports = {
        generateUUID : generateUUID,
        generateFileName: generateFileName,
        generateFullFilePath: generateFullFilePath
    };

})()
