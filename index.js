
/**
 * Module dependencies.
 */

var express = require('express');
var logger = require('morgan');
var router = require('./routes');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var serveStatic = require('serve-static');
var fs = require('fs');

var app = express();

var cors = require('./cors');
app.use(cors());

// Configuration
app.use(bodyParser.json());

// override with POST having ?_method=DELETE
app.use(methodOverride('_method'));
//TODO configurar arquivos estáticos para a versão web
//  app.use(express.static(__dirname + '/public'))

//TODO mover configuração para index da pasta /public
//TODO adicionar estratégia de cache e headers
//TODO deixar nginx
var options = {
    index: false,
    maxAge: '1d',
    redirect: false,
    setHeaders: function (res, path, stat) {
      res.set('x-timestamp', Date.now());
    }
  };

if (!fs.existsSync(__dirname + '/uploads')){
    fs.mkdirSync(__dirname + '/uploads');
}

app.use('/public/certificados',serveStatic(__dirname + '/uploads', options));
app.use('/', router);

//app.use('/teste', teste)

if('development' === app.get('env')){
  //development only
  //var errorHandler = require('errorhandler');
  //app.use(errorHandler({ dumpExceptions: true, showStack: true }))
  app.use(logger('dev'));
}

if('production' ===  app.get('env')){
  //app.use()
  app.use(logger('common'));
  //todo usar middleware para tratar erros
}

app.use(function(err, req, res, next) {

  console.error("erro interno", err.stack);

  return res.status(500).send({ error: err});

});

var server = app.listen(process.env.PORT || 3000 , function(){
  console.log("Express server listening on port %d in %s mode", server.address().port, app.settings.env);
});

module.exports = app;
