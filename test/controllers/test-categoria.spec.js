var expect = require('chai').expect;
var router = require('../../routes/index');
var request = require('supertest');
var app = require('../../index');
var Categoria = require('../../model/Categoria');

var categoria = {_id: "58095b436c9e1a3863d5d751", nome: "Eventos", maxHoras: 64};
var tokenAdmin = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6ImFkbWluIiwiX2lkIjoiNTgwOTViNjYyNDAyOWEzOGU4MzAzZWQ5In0.8V-V3YXkIUVXHQhhb-yr3s_MOsuwGqRf3x1293jBd20";

describe('Criação de categoria', function(){

  beforeEach(function(done){

    Categoria.remove({}, done);
  });

  it('Token inválido', function(done){

    request(app)
    .post('/admin/categoria')
    .send({categoria:categoria})
    .expect(401)
    .end(done);

  });

  it('Categoria criada', function(done){

    request(app)
    .post('/admin/categoria')
    .set('X-Auth', tokenAdmin)
    .send({categoria:categoria})
    .expect(200)
    .end(done);

  });

  it('Parâmetros inválidos', function(done){

    request(app)
    .post('/admin/categoria')
    .set('X-Auth', tokenAdmin)
    .expect(400)
    .end(done);

  });

  it('Erro na categoria', function(done){
    var categoriaError = {_id : "1234", nome: "test", maxHoras: 10};

    request(app)
    .post('/admin/categoria')
    .set('X-Auth', tokenAdmin)
    .send({categoria:categoriaError})
    .expect(500)
    .end(done);

  });

});


describe('Edição de categoria', function(){

  beforeEach(function(done){

    Categoria.remove({}, function(err, result){

      if(err){
        done(err, result);
      }

      var categoriaModel = new Categoria(categoria);

      categoriaModel.save(done);

    });


  });

  it('Token inválido', function(done){

    request(app)
    .put('/admin/categoria')
    .send({categoria:categoria})
    .expect(401)
    .end(done);

  });

  it('Editar categoria criada', function(done){

    categoria.nome = "Eventos Locais";

    request(app)
    .put('/admin/categoria')
    .set('X-Auth', tokenAdmin)
    .send({categoria:categoria})
    .expect(200)
    .end(done);

  })

  it('Parâmetros inválidos', function(done){

    request(app)
    .put('/admin/categoria')
    .set('X-Auth', tokenAdmin)
    .expect(400)
    .end(done);

  });

  it('Erro na categoria', function(done){

    var categoriaError = {_id : "1234", nome: "test", maxHoras: 10};

    request(app)
    .put('/admin/categoria')
    .set('X-Auth', tokenAdmin)
    .send({categoria:categoriaError})
    .expect(204)
    .end(done);

  });

  it('Erro nos dados a serem atualizados', function(done){

    categoria.maxHoras = "true";

    request(app)
    .put('/admin/categoria')
    .set('X-Auth', tokenAdmin)
    .send({categoria:categoria})
    .expect(500)
    .end(done);

  });

});


describe('Busca de categoria', function(){

  it('Busca de categoria', function(done){

    var query = {query:{nome: "Eventos Locais"}};

    request(app)
    .post('/aluno/categoria')
    .send({query:query})
    .expect(200)
    .end(done);

  })

  it('Query inválida', function(done){

    var query = {sort: 1253465234};

    request(app)
    .post('/aluno/categoria')
    .send(query)
    .expect(500)
    .end(done);

  });

  it('Skip inválido', function(done){

    var query = {skip: -100};

    request(app)
    .post('/aluno/categoria')
    .send(query)
    .expect(500)
    .end(done);

  });

});
