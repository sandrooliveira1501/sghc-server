var expect = require('chai').expect;
var router = require('../../routes/index');
var request = require('supertest');
var bcrypt = require('bcrypt');
var jwt = require('jwt-simple');
var Administrador = require('../../model/Administrador');
var config = require('../../config');
var app = require('../../index');

var admin = {login:"admin", senha: "admin"};


describe('AUTH Administrador /', function(){

  beforeEach(function(done){

    Administrador.remove({}, function(err, result){

      if(err){
        done(err, result);
      }
      bcrypt.hash(admin.senha, config.saltRoundsBCrypt, function(err, hash) {

        if(err){
          done(err, result);
        }

        var adminModel = new Administrador(admin);
        adminModel.senha = hash;
        adminModel.save(done);

      });

    });

  });

  it('Autenticação válida com token ', function(done){

    request(app)
    .post('/admin/auth')
    .send(admin)
    .expect('Content-Type', /json/)
    .expect(200)
    .expect(hasPreviousAndNextKeys)
    .end(done);

    function hasPreviousAndNextKeys(res) {
      if (!('token' in res.body)) throw new Error("token não existe na resposta");
      try{
          console.log("token admin", res.body.token);
          jwt.decode(res.body.token, config.secretKeyAdmin);
      }catch(err){
          throw new Error("token inválido");
      }
    }

  });

  it('Parâmetros inválidos', function(done){

    request(app)
    .post('/admin/auth')
    .expect(400)
    .end(done);

  });

  it('Erro no mongoose', function(done){
    var adminTest = {login: "admin", senha: 1234}

    request(app)
    .post('/admin/auth')
    .send(adminTest)
    .expect(500)
    .end(done);

  });


  it('Erro de autenticação', function(done){
    var adminTest = {login: "admin", senha: "1234"}
    request(app)
    .post('/admin/auth')
    .send(adminTest)
    .expect(401)
    .end(done);

  })


  it('rota existe', function(done){

    request(app)
      .post('/admin/auth')
      .send(admin)
      .expect(200)
      .end(done)
  });

});
