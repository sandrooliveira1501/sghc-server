var expect = require('chai').expect;
var router = require('../../routes/index');
var request = require('supertest');
var app = require('../../index');
var Atividade = require('../../model/Atividade');

var atividade = { _id: "58076c527c4d2f5e53b9f391",
                  aluno: "58076c527c4d2f5e53b9f391",
                  descricao: "Flisol 2016",
                  categoria: "58095b436c9e1a3863d5d751",
                  horas: 10,
                  boolean: false
                }
var tokenAluno = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtYXRyaWN1bGEiOjExMTExMSwiX2lkIjoiNTgwNzZjNTI3YzRkMmY1ZTUzYjlmMzkxIn0.JrOiHK1A_TPHQktWLoOscA39VLWB8g3HOKhGN3GE9P8"

describe('Criação de atividade', function(){

  beforeEach(function(done){

    Atividade.remove({}, done);
  });

  it('Token inválido', function(done){

    request(app)
    .post('/aluno/atividade')
    .send({atividade:atividade})
    .expect(401)
    .end(done);

  });

  it('Atividade criada', function(done){

    request(app)
    .post('/aluno/atividade')
    .set('X-Auth', tokenAluno)
    .send({atividade:atividade})
    .expect(200)
    .end(done);

  });

  it('Parâmetros inválidos', function(done){

    request(app)
    .post('/aluno/atividade')
    .set('X-Auth', tokenAluno)
    .expect(400)
    .end(done);

  });

  it('Erro na atividade', function(done){
    var atividadeError = {_id : "1234"};

    request(app)
    .post('/aluno/atividade')
    .set('X-Auth', tokenAluno)
    .send({atividade:atividadeError})
    .expect(500)
    .end(done);

  });

});


describe('Edição de atividade', function(){

  beforeEach(function(done){

    Atividade.remove({}, function(err, result){

      if(err){
        done(err, result);
      }

      var atividadeModel = new Atividade(atividade);

      atividadeModel.save(done);

    });


  });

  it('Token inválido', function(done){

    request(app)
    .put('/aluno/atividade')
    .send({atividade:atividade})
    .expect(401)
    .end(done);

  });

  it('Editar atividade criada', function(done){

    atividade.nome = "FLISoL 2016";

    request(app)
    .put('/aluno/atividade')
    .set('X-Auth', tokenAluno)
    .send({atividade:atividade})
    .expect(200)
    .end(done);

  })

  it('Parâmetros inválidos', function(done){

    request(app)
    .put('/aluno/atividade')
    .set('X-Auth', tokenAluno)
    .expect(400)
    .end(done);

  });

  it('Erro no atividade', function(done){

    var atividadeError = {_id : "1234"};

    request(app)
    .put('/aluno/atividade')
    .set('X-Auth', tokenAluno)
    .send({atividade:atividadeError})
    .expect(204)
    .end(done);

  });

  it('Erro nos dados a serem atualizados', function(done){

    atividade.horas = "true";

    request(app)
    .put('/aluno/atividade')
    .set('X-Auth', tokenAluno)
    .send({atividade:atividade})
    .expect(500)
    .end(done);

  });

});


describe('Busca de atividade', function(){

  it('Busca de atividade', function(done){

    var query = {query:{descricao: "FLISoL"}};

    request(app)
    .post('/aluno/atividade/search')
    .set('X-Auth', tokenAluno)
    .send({query:query})
    .expect(200)
    .end(done);

  })

  it('Query inválida', function(done){

    var query = {sort: 1253465234};

    request(app)
    .post('/aluno/atividade/search')
    .set('X-Auth', tokenAluno)
    .send(query)
    .expect(500)
    .end(done);

  });

  it('Skip inválido', function(done){

    var query = {skip: -100};

    request(app)
    .post('/aluno/atividade/search')
    .set('X-Auth', tokenAluno)
    .send(query)
    .expect(500)
    .end(done);

  });

});



describe('Remoção de atividade', function(){

  it('Remoção válida', function(done){

    var params = {idAtividade: atividade._id};

    request(app)
    .delete('/aluno/atividade')
    .set('X-Auth', tokenAluno)
    .send(params)
    .expect(200)
    .end(done);

  })

  it('Parâmetros inválidos', function(done){


    request(app)
    .delete('/aluno/atividade')
    .set('X-Auth', tokenAluno)
    .expect(400)
    .end(done);

  });

  it('Erro interno', function(done){

    var params = {idAtividade: 1234};

    request(app)
    .delete('/aluno/atividade')
    .set('X-Auth', tokenAluno)
    .send(params)
    .expect(500)
    .end(done);

  });

});
