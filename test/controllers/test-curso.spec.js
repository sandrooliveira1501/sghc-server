var expect = require('chai').expect;
var router = require('../../routes/index');
var request = require('supertest');
var app = require('../../index');
var Curso = require('../../model/Curso');

var curso = {_id: "58095b436c9e1a3863d5d751", nome: "SI", quantidadeDeHoras: 256};
var tokenAdmin = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6ImFkbWluIiwiX2lkIjoiNTgwOTViNjYyNDAyOWEzOGU4MzAzZWQ5In0.8V-V3YXkIUVXHQhhb-yr3s_MOsuwGqRf3x1293jBd20";

describe('Criação de curso', function(){

  beforeEach(function(done){

    Curso.remove({}, done);
  });

  it('Token inválido', function(done){

    request(app)
    .post('/admin/curso')
    .send({curso:curso})
    .expect(401)
    .end(done);

  });

  it('Curso criado', function(done){

    request(app)
    .post('/admin/curso')
    .set('X-Auth', tokenAdmin)
    .send({curso:curso})
    .expect(200)
    .end(done);

  });

  it('Parâmetros inválidos', function(done){

    request(app)
    .post('/admin/curso')
    .set('X-Auth', tokenAdmin)
    .expect(400)
    .end(done);

  });

  it('Erro no curso', function(done){
    var cursoError = {_id : "1234", nome: "test", quantidadeDeHoras: 100};

    request(app)
    .post('/admin/curso')
    .set('X-Auth', tokenAdmin)
    .send({curso:cursoError})
    .expect(500)
    .end(done);

  });

});


describe('Edição de curso', function(){

  beforeEach(function(done){

    Curso.remove({}, function(err, result){

      if(err){
        done(err, result);
      }

      var cursoModel = new Curso(curso);

      cursoModel.save(done);

    });


  });

  it('Token inválido', function(done){

    request(app)
    .put('/admin/curso')
    .send({curso:curso})
    .expect(401)
    .end(done);

  });

  it('Editar curso criada', function(done){

    curso.nome = "Sistemas de Informação";

    request(app)
    .put('/admin/curso')
    .set('X-Auth', tokenAdmin)
    .send({curso:curso})
    .expect(200)
    .end(done);

  })

  it('Parâmetros inválidos', function(done){

    request(app)
    .put('/admin/curso')
    .set('X-Auth', tokenAdmin)
    .expect(400)
    .end(done);

  });

  it('Erro no curso', function(done){

    var cursoError = {_id : "1234", nome: "test", quantidadeDeHoras: 100};

    request(app)
    .put('/admin/curso')
    .set('X-Auth', tokenAdmin)
    .send({curso:cursoError})
    .expect(204)
    .end(done);

  });

  it('Erro nos dados a serem atualizados', function(done){

    curso.quantidadeDeHoras = "true";

    request(app)
    .put('/admin/curso')
    .set('X-Auth', tokenAdmin)
    .send({curso:curso})
    .expect(500)
    .end(done);

  });

});


describe('Busca de curso', function(){

  it('Busca de curso', function(done){

    var query = {query:{nome: "SI"}};

    request(app)
    .post('/aluno/curso')
    .send({query:query})
    .expect(200)
    .end(done);

  })

  it('Query inválida', function(done){

    var query = {sort: 1253465234};

    request(app)
    .post('/aluno/curso')
    .send(query)
    .expect(500)
    .end(done);

  });

  it('Skip inválido', function(done){

    var query = {skip: -100};

    request(app)
    .post('/aluno/curso')
    .send(query)
    .expect(500)
    .end(done);

  });

});
