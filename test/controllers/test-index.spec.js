var expect = require('chai').expect;
var router = require('../../routes/index');
var request = require('supertest');
var app = require('../../index');

describe('routes.index', function(){

  it('exists', function(){
    expect(router).to.exist
  });

});

describe('GET /', function(){

  it('responde com OK', function(done){

    request(app)
    .get('/')
    .query({ query: 'test' })
    .expect('Content-Type', /json/)
    .expect({message:"Ok"}, done);

  });
});

describe('GET / existe', function(){

  it('exists', function(done){

    request(app)
      .get('/')
      .expect(200)
      .end(done)
  });

})
