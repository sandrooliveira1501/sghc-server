var expect = require('chai').expect;
var router = require('../../routes/index');
var request = require('supertest');
var bcrypt = require('bcrypt');
var jwt = require('jwt-simple');
var Aluno = require('../../model/Aluno');
var Administrador = require('../../model/Administrador');
var config = require('../../config');
var app = require('../../index');

var aluno = {_id: "58076c527c4d2f5e53b9f391",
              nome:"Aluno Teste", matricula: 111111,
              senha: "test", email:"teste@test.com",
              curso: "112345679065574883030833"};
var alunoTest = {matricula: 222222, nome:"Aluno Teste",
                  senha: "test", email:"teste2@test.com",
                   curso: "112345679065574883030833"};
var tokenAluno = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtYXRyaWN1bGEiOjExMTExMSwiX2lkIjoiNTgwNzZjNTI3YzRkMmY1ZTUzYjlmMzkxIn0.JrOiHK1A_TPHQktWLoOscA39VLWB8g3HOKhGN3GE9P8"

describe('AUTH Aluno /', function(){

  beforeEach(function(done){

    Aluno.remove({}, function(err, result){

      if(err){
        done(err, result);
      }
      bcrypt.hash(aluno.senha, config.saltRoundsBCrypt, function(err, hash) {

        if(err){
          done(err, result);
        }

        var alunoModel = new Aluno(aluno);
        alunoModel.senha = hash;
        alunoModel.save(done);

      });

    });

  });

  it('Autenticação válida com token ', function(done){

    request(app)
    .post('/aluno/auth')
    .send(aluno)
    .expect('Content-Type', /json/)
    .expect(200)
    .expect(hasPreviousAndNextKeys)
    .end(done);

    function hasPreviousAndNextKeys(res) {
      if (!('token' in res.body)) throw new Error("token não existe na resposta");
      try{
          console.log("token", res.body.token);
          jwt.decode(res.body.token, config.secretKeyAluno);
      }catch(err){
          throw new Error("token inválido");
      }
    }

  });

  it('Parâmetros inválidos', function(done){

    request(app)
    .post('/aluno/auth')
    .expect(400)
    .end(done);

  });

  it('Erro no mongoose', function(done){
    var alunoTest = {matricula: "teste", senha: "1234"}

    request(app)
    .post('/aluno/auth')
    .send(alunoTest)
    .expect(500)
    .end(done);

  });

  it('Erro de autenticação', function(done){
    var alunoTest = {matricula: 111111, senha: "1234"}
    request(app)
    .post('/aluno/auth')
    .send(alunoTest)
    .expect(401)
    .end(done);

  });


  it('rota existe', function(done){

    request(app)
      .post('/aluno/auth')
      .send(aluno)
      .expect(200)
      .end(done)
  });

});

describe('Auth / Criação de aluno', function(){

  it('parâmetros inválidos', function(done){

    request(app)
      .post('/aluno/new')
      .send(alunoTest)
      .expect(400)
      .end(done)
  });

  it('parâmetros válidos', function(done){

    request(app)
      .post('/aluno/new')
      .send({aluno:alunoTest})
      .expect(200)
      .end(done)
  });

  it('aluno já existe', function(done){

    request(app)
      .post('/aluno/new')
      .send({aluno:alunoTest})
      .expect(500)
      .end(done)
  });


  it('senha inválida', function(done){

      request(app)
        .post('/aluno/new')
        .send({aluno:{}})
        .expect(500)
        .end(done)
  });

})


describe('Edição de aluno / Edição de dados de aluno', function(){

  it('token inexistente ou token inválido', function(done){

    request(app)
      .post('/aluno/update')
      //.set('X-Auth', token)
      .send(alunoTest)
      .expect(401)
      .end(done)
  });

  it('edição de aluno', function(done){

    request(app)
      .post('/aluno/update')
      .set('x-auth', tokenAluno)
      .send({aluno:aluno})
      .expect(200)
      .end(done)
  });

  it('parâmetros inválidos', function(done){

    request(app)
      .post('/aluno/update')
      .set('x-auth', tokenAluno)
      .expect(400)
      .end(done)
  });

  it('parâmetros inválidos', function(done){
    var params = {aluno:{_id:"58076c527c4d2f5e53b9f392"}};

    request(app)
      .post('/aluno/update')
      .set('x-auth', tokenAluno)
      .send(params)
      .expect(204)
      .end(done)
  });

  it('matrícula já existente', function(done){

    aluno.matricula = "222222";

    request(app)
      .post('/aluno/update')
      .set('x-auth', tokenAluno)
      .send({aluno:aluno})
      .expect(500)
      .end(done)
  });

})


describe('Busca de aluno: ', function(){

  it('token inexistente ou token inválido', function(done){

    request(app)
      .get('/aluno/')
      //.set('X-Auth', token)
      .expect(401)
      .end(done)
  });

  it('aluno existe', function(done){

    request(app)
      .get('/aluno')
      .set('x-auth', tokenAluno)
      .expect(200)
      .end(done)
  });

  it('aluno não existe', function(done){

    request(app)
      .get('/aluno')
      .set('x-auth', tokenAluno)
      .query({ matricula:1212})
      .expect(204)
      .end(done)
  });


})


describe('Alterar senha: ', function(){

  it('parâmetros inválidos', function(done){

    request(app)
      .post('/aluno/password')
      .set('X-Auth', tokenAluno)
      .expect(400)
      .end(done)
  });

  it('aluno existe', function(done){
    aluno.novaSenha = "1234";
    request(app)
      .post('/aluno/password')
      .set('x-auth', tokenAluno)
      .send({aluno:aluno})
      .expect(200)
      .end(done)
  });

  it('aluno não existe', function(done){
    aluno._id = "1234";
    request(app)
      .post('/aluno/password')
      .set('x-auth', tokenAluno)
      .send({aluno:aluno})
      .expect(204)
      .end(done)
  });

})
