var expect = require('chai').expect;
var router = require('../../routes/index');
var request = require('supertest');
var app = require('../../index');
var Atividade = require('../../model/Atividade');

var atividade = { _id: "58076c527c4d2f5e53b9f391",
                  aluno: "58076c527c4d2f5e53b9f391",
                  descricao: "Flisol 2016",
                  categoria: "58095b436c9e1a3863d5d751",
                  horas: 10,
                  boolean: false
                }

var tokenAluno = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtYXRyaWN1bGEiOjExMTExMSwiX2lkIjoiNTgwNzZjNTI3YzRkMmY1ZTUzYjlmMzkxIn0.JrOiHK1A_TPHQktWLoOscA39VLWB8g3HOKhGN3GE9P8"

var tokenAluno2 = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtYXRyaWN1bGEiOjExMTExMSwiX2lkIjoiNTgwNzZjNTI3YzRkMmY1ZTUzYjlmMzkzIn0.cjnTdRKWTr4B7ibBGDSjgEKlnvxbSiSsCAB51W75sxE"

describe('Envio de Certificado: ', function(){

  beforeEach(function(done){

    Atividade.remove({}, function(err, result){

      if(err){
        done(err, result);
      }

      var atividadeModel = new Atividade(atividade);

      atividadeModel.save(done);

    });


  });

  it('paramêtros inválidos', function(done){
    request(app)
    .post('/aluno/atividade/certificado')
    .set('X-Auth', tokenAluno)
    .expect(400)
    .end(done);
  });

  it('atividade inexistente', function(done){
    request(app)
    .post('/aluno/atividade/certificado')
    .set('X-Auth', tokenAluno)
    .field('idAtividade', '58076c527c4d2f5e53b9f392')
    .expect(204)
    .end(done);
  });

  it('erro de permissão', function(done){
    request(app)
    .post('/aluno/atividade/certificado')
    .set('X-Auth', tokenAluno2)
    .field('idAtividade', atividade._id)
    .expect(401)
    .end(done);
  });

  it('sem arquivo', function(done){
    request(app)
    .post('/aluno/atividade/certificado')
    .set('X-Auth', tokenAluno)
    .field('idAtividade', atividade._id)
    .expect(400)
    .end(done);
  });

  it('arquivo válido', function(done){
    request(app)
    .post('/aluno/atividade/certificado')
    .set('X-Auth', tokenAluno)
    .field('idAtividade', atividade._id)
    .attach('file', 'test/test.pdf')
    .expect(200)
    .end(done);
  });

  it('erro arquivo', function(done){
    request(app)
    .post('/aluno/atividade/certificado')
    .set('X-Auth', tokenAluno)
    .field('idAtividade', atividade._id)
    .field('simulateErrorFile', "true")
    .attach('file', 'test/test.pdf')
    .expect(500)
    .end(done);
  });

  it('erro banco', function(done){
    request(app)
    .post('/aluno/atividade/certificado')
    .set('X-Auth', tokenAluno)
    .field('idAtividade', atividade._id)
    .field('simulateErrorDatabase', "true")
    .attach('file', 'test/test.pdf')
    .expect(500)
    .end(done);
  });


});
