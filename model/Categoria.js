var mongoose = require('../db.js');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var constants = {
  MODEL_NAME: 'Categoria',
  COLLECTION_NAME: 'categorias'
}

var schema = new Schema({
  nome: {type:String, required:true},
  maxHoras: {type:Number, required:true}
}, {collection: constants.COLLECTION_NAME})


var Categoria  = mongoose.model(constants.MODEL_NAME, schema)
Categoria.constants = constants

module.exports = Categoria;
