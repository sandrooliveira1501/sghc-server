var mongoose = require('../db.js');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var constants = {
  MODEL_NAME: 'Atividade',
  COLLECTION_NAME: 'atividades'
}

var schema = new Schema({
  aluno: {type:ObjectId, required: true},
  descricao: {type:String, required:true},
  categoria: {type:ObjectId, required: true},
  horas: {type:Number, required:true, default: 0},
  data: {type: Date, default: Date.now},
  boolean: {type: Boolean, default: false},
  arquivo: {type: String, default: ""}
}, {collection: constants.COLLECTION_NAME})


var Atividade  = mongoose.model(constants.MODEL_NAME, schema)
Atividade.constants = constants

module.exports = Atividade;
