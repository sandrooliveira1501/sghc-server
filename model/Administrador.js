// grab the things we need
var mongoose = require('../db.js');
var bcrypt = require('bcrypt')
var jwt = require('jwt-simple')
var config = require('../config')
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var constants = {
  ADMIN_ROLE: 'ADMIN',
  MODEL_NAME: 'Administrador',
  COLLECTION_NAME: 'administradores'
}

// create a schema
var schema = new Schema({
  login: {type: String, required: true, unique: true},
  senha: {type: String, required: true, select: false} //todo select false
}, {collection: constants.COLLECTION_NAME});

// the schema is useless so far
// we need to create a model using it

schema.statics.authenticateUser = function(login, senha, cb){

  var query = {login : login}

  this.
  findOne(query, {_id:1, login:1, senha:1}).
  exec(function(err, admin){

    if(err) {return cb(err, 500)}

    if(!admin) {return cb(null,401)} //Unauthorized

    bcrypt.compare(senha, admin.senha,
        function(err, valid){

          if(err) {return cb(err, 500)}

          if(!valid) {return cb(null,401)} //Unauthorized

          var payload = {
              login : admin.login,
              _id : admin._id
              }

          var secretKey = config.secretKeyAdmin

          var token = jwt.encode(payload, secretKey)

          cb(null,201,token)
        }
    )

  })

}

var Administrador = mongoose.model(constants.MODEL_NAME, schema)
Administrador.constants = constants

// exportando módulo
module.exports = Administrador
