var mongoose = require('../db.js');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var constants = {
  MODEL_NAME: 'Curso',
  COLLECTION_NAME: 'cursos'
}

var schema = new Schema({
  nome: {type:String, required:true},
  quantidadeDeHoras: {type:Number, required:true},
  categorias: [ObjectId]
}, {collection: constants.COLLECTION_NAME})


var Curso  = mongoose.model(constants.MODEL_NAME, schema)
Curso.constants = constants

module.exports = Curso;
