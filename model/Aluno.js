// grab the things we need
var mongoose = require('../db.js');
var bcrypt = require('bcrypt')
var config = require('../config')
var jwt = require('jwt-simple')
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var constants = {
  MODEL_NAME: 'Aluno',
  COLLECTION_NAME: 'alunos'
}

// create a schema
var schema = new Schema({
  matricula: {type: Number, required: true, unique: true},
  nome: {type: String, required: true},
  senha: {type: String, required: true, select: false},
  email: {type: String, required: true, unique: true},
  curso: {type: ObjectId, required: true},
}, {collection: constants.COLLECTION_NAME});

// the schema is useless so far
// we need to create a model using it

schema.statics.authenticateUser = function(matricula, senha, cb){

  var query = {matricula : matricula}

  this.
  findOne(query, {_id:1, matricula:1, senha:1}).
  exec(function(err, aluno){

    if(err) {return cb(err, 500)}

    console.log("aluno", aluno);

    if(!aluno) {return cb(null,401)} //Unauthorized

    bcrypt.compare(senha, aluno.senha,
        function(err, valid){

          if(err) {return cb(err)}

          if(!valid) {return cb(null,401)} //Unauthorized

          var payload = {
              matricula : aluno.matricula,
              _id : aluno._id
              }

          var secretKey = config.secretKeyAluno

          var token = jwt.encode(payload, secretKey)

          cb(null,201,token)
        }
    )

  })

}

schema.statics.alterarSenha = function(paramAluno, cb){

  var query = {_id : paramAluno._id}

  //busca aluno no banco
  this.
  findOne(query, {_id:1, matricula:1, senha:1}).
  exec(function(err, aluno){

    if(err) {return cb(err, 500)}

    console.log("aluno", aluno);

    if(!aluno) {return cb(null,401)} //Unauthorized

    //compara senha antiga
    bcrypt.compare(paramAluno.senha, aluno.senha,
        function(err, valid){

          if(err) {return cb(err)}

          if(!valid) {return cb(null,401)} //Unauthorized

          //faz hash da nova senha
          bcrypt.hash(paramAluno.novaSenha, config.saltRoundsBCrypt, function(err, hash) {

            //verifica erros
            if(err){
              cb(err);
            }

            //salva aluno no banco
            aluno.senha = hash;
            aluno.save(function(err, alunoSaved){

              if(err){
                return cb(err);
              }

              cb(null,200,{message: "Ok"});
            });

          });

        }
    )

  })

}

var aluno = mongoose.model(constants.MODEL_NAME, schema);
aluno.constants = constants;

// exportando módulo
module.exports = aluno;
